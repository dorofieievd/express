import { Request, Response, NextFunction } from 'express';

const controllerWrapper = (requestHandler: any) => {
  return async (req: Request, res: Response, next: NextFunction) => {
    try {
      await requestHandler(req, res, next);
    } catch (error) {
      next(error);
    }
  };
};

export default controllerWrapper;
