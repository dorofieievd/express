import express from 'express';
import cors from 'cors';
import logger from './middlewares/logger.middleware';
import studentsRouter from '../students/students.router';
import bodyParser from 'body-parser';
import exceptionFilter from './middlewares/exceptions.filter';
import { AppDataSource } from '../configs/database/data-source';

const app = express();

app.use(cors());
app.use(bodyParser.json());
app.use(logger);
AppDataSource.initialize()
  .then(() => {
    console.log('Typeorm connected to database');
  })
  .catch((error) => {
    console.log('Error: ', error);
  });

app.use('/api/v1/students', studentsRouter);

app.use(exceptionFilter);

export default app;
