import { Request, Response } from 'express';
import * as studentsService from './students.service';
import { IStudentUpdateRequest } from './types/student-update-request.interface';
import { ValidatedRequest } from 'express-joi-validation';
import { IStudentCreateRequest } from './types/student-create-request.interface';

export const getAllStudents = async (req: Request, res: Response) => {
  const students = await studentsService.getAllStudents();
  res.json(students);
};

export const getStudentById = async (req: Request, res: Response) => {
  const { id } = req.params;
  const student = await studentsService.getStudentById(id);
  res.json(student);
};

export const createStudent = async (
  req: ValidatedRequest<IStudentCreateRequest>,
  res: Response
) => {
  console.log(req.body);
  const student = await studentsService.createStudent(req.body);
  res.json(student);
};

export const updateStudentById = async (
  req: ValidatedRequest<IStudentUpdateRequest>,
  res: Response
) => {
  const { id } = req.params;
  const student = await studentsService.updateStudentById(id, req.body);
  res.json(student);
};

export const deleteStudentById = async (req: Request, res: Response) => {
  const { id } = req.params;
  const student = await studentsService.deleteStudentById(id);
  res.json(student);
};
