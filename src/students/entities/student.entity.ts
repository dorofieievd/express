import { Column, Entity } from 'typeorm';
import { CoreEntity } from '../../application/entities/core.entity';

@Entity({ name: 'students' })
export class Student extends CoreEntity {
  @Column({
    type: 'varchar',
    nullable: false,
  })
  name: string;

  @Column({
    type: 'numeric',
    nullable: false,
  })
  age: number;

  @Column({
    type: 'varchar',
    nullable: false,
  })
  surname: string;

  @Column({
    type: 'varchar',
    nullable: false,
  })
  email: string;
}
